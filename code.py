# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 13:41:03 2020

@author: IDUXNEID
required libraries: whois,itertools,time,random
note:Please download and install those libraries if you don't have them.
note:Don't forget to adjust the settings as you wish.
"""
from sys import platform
import whois
import itertools
import time 
import random
############################## S E T T I N G S #################################
char_types='abcdefghijklmnopqrstuvwxyz0123456789' #Char dictionary, if you dont want, drag out
domain_suffix='com' #Which somain suffix will be added to search
char_length=4 #Number of the length of the strings will be generated
delay_time=5 #Choose the delay time in seconds, because you don't want to get banned from whois server
show_me_only_empty=False #Make it True if you only want to see the empty domains, otherwise it sould be False
shuffle_domain_names=True #Make it True if you want the doman names shuffled before control.
################################################################################
domain_list = [] #Domain name list will be filled automatically
found_empty=[] #Empty domain list will be filled automatically
found_counter=0 #Found empty domain counter will be filled automatically
if not char_types.isalnum():
    print('Control your char types, it only can include alphanumeric characters.')
    raise SystemExit
print('Domain name generation phase has been started...')
try:
    start_generation_time=time.time()
    for single_row in itertools.product(char_types,repeat=char_length):
        domain_list.append(str(''.join(single_row)+'.'+domain_suffix))
    end_generation_time=time.time()
except:
    print('Error at name generation phase')
print('Domain list generation phase finished successfully.')
if shuffle_domain_names:
    random.shuffle(domain_list)
    print('Domain name list shuffled.')
domain_generation_time=end_generation_time-start_generation_time
print('Domain name generation phase took',end_generation_time-start_generation_time,'seconds.')
print('Dont forget, you can close the operation by pressing Ctrl+C at your console,it is not going to close itself.')
print('Domain name searching phase is starting...')
print('You can access found empty domain names by looking at the variable as named found_empty')
control_time_start=time.time()
for dom in domain_list:
    time.sleep(delay_time) #Sleeping because, whois library may have refuse us if we try so much
    try:
        if platform == "linux" or platform == "linux2":
            w = whois.query(dom)
            if not w.registrar:
                print('Empty: ',dom)
                found_empty.append(dom)
                found_counter+=1
            else:
                if not show_me_only_empty:
                    print('Exist: ',dom)
        elif platform == "darwin":
            w = whois.query(dom)
            if not w.registrar:
                print('Empty: ',dom)
                found_empty.append(dom)
                found_counter+=1
            else:
                if not show_me_only_empty:
                    print('Exist: ',dom)
        elif platform == "win32":
            w = whois.whois(dom)
            if not w.registrar:
                print('Empty: ',dom)
                found_empty.append(dom)
                found_counter+=1
            else:
                if not show_me_only_empty:
                    print('Exist: ',dom)
    except KeyboardInterrupt as e: #Needed because i figured that you can not use Ctrl+C when a loop is runnin in Python
        print('Operation closed by the user.')
        print('Here is the list of all found empty',found_counter,'domains so far:')
        print(*found_empty, sep='\n')
        raise SystemExit
    except:
        print('Empty: ',dom)
        found_empty.append(dom)
        found_counter+=1
control_time_end=time.time()
control_time=control_time_end-control_time_start
print('Control operation is finished at',control_time,'seconds')
print('Here is the list  of all empty',found_counter,'domains that has been found:')
print(*found_empty, sep='\n')